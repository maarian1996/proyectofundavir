﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capaDatos
{
    public class DBConexión
    {
        public static SqlConnection ObtenerConexion()
        {
            SqlConnection Conn = new SqlConnection(ConfigurationManager.AppSettings["Connec"].ToString());
            Conn.Open();
            return Conn;
        }
    }
}
