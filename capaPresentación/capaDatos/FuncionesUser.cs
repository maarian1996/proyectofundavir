﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace capaDatos
{
    public class FuncionesUser
    {
        public static int CrearCuentas(string pUsuario, string pContrasena, string pRol)
        {
            int resultado = 0;
            SqlConnection Conn = DBConexión.ObtenerConexion();
            SqlCommand Comando = new SqlCommand();
            //SqlCommand Comando = new SqlCommand(string.Format("Insert Into Usuarios (Nombre, Contraseña) values ('{0}', PwdEncrypt('{1}') )", pUsuario, pContraseña), Conn);
            Comando.Connection = Conn;
            Comando.CommandType = CommandType.StoredProcedure;
            Comando.CommandText = "InsertarUsuario";
            Comando.Parameters.AddWithValue("@v_usuario", pUsuario);
            Comando.Parameters.AddWithValue("@v_pass", Encriptar.GetMD5(pContrasena));
            Comando.Parameters.AddWithValue("@v_rol", pRol);
            Comando.Parameters.AddWithValue("@v_bloqueo", DateTime.Now);
            Comando.Parameters.AddWithValue("@v_baja", true);
            Comando.ExecuteNonQuery();
            Conn.Close();
            return resultado;
        }

        public static int Autentificar(String pUsuarios, String pContrasena)
        {
            int resultado = 0;
            SqlConnection conexión = DBConexión.ObtenerConexion();
            //SqlCommand comando = new SqlCommand(string.Format("Select * From Usuario Where nombre ='{0}' and pass = '{1}'", pUsuarios, pContrasena), conexión);
            SqlCommand comando = new SqlCommand();
            comando.Connection = conexión;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "AutentificarUsuario";
            comando.Parameters.AddWithValue("@v_usuario", pUsuarios);
            comando.Parameters.AddWithValue("@v_pass", pContrasena);
            SqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                resultado = 50;
            }
            conexión.Close();
            return resultado;
        }
        public int ModificarUsuario(string pUsuario, string pContrasena, string pRol)
        {
            int resultado = 0;
            SqlConnection conn = DBConexión.ObtenerConexion();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "ModificarUsuario";
            cmd.Parameters.AddWithValue("@v_usuario", pUsuario);
            cmd.Parameters.AddWithValue("@v_pass", pContrasena);
            cmd.Parameters.AddWithValue("@v_rol", pRol);
            cmd.ExecuteNonQuery();
            conn.Close();
            return resultado;
        }
        public int BajaUser(string pDown)
        {
            int re = 0;
            SqlConnection cnn = DBConexión.ObtenerConexion();
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = cnn;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "BajaUsuario";
            cmd.Parameters.AddWithValue("@v_baja", pDown);
            cmd.ExecuteNonQuery();
            cnn.Close();
            return re;
        }
        
    }
}
