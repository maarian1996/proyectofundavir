﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace capaEntidades
{
    public class Users
    {
        public int id { get; set; }
        public string Usuario { get; set; }
        public string Contra { get; set; }
        public bool Down { get; set; }
        public string Rol { get; set; }
        public string FechaBloqueo { get; set; }
        public Users() { }
        public Users(int pId, string pUsuarios, string pContrasena, string pRol, string pFecha)
        {
            id = pId;
            Usuario = pUsuarios;
            Contra = pContrasena;
            Rol = pRol;
            FechaBloqueo = pFecha;
        }
    }
}
