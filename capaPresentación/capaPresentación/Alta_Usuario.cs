﻿using capaDatos;
using capaEntidades;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace capaPresentación
{
    public partial class Alta_Usuario : Form
    {
        public Alta_Usuario()
        {
            InitializeComponent();
        }

        void clear()
        {
            txtConfirmar.Text = null;
            txtContraseña.Text = null;
            txtUsuario.Text = null;
            comboBox1.SelectedIndex = -1;
        }

        private void btnRegistrar_Click(object sender, EventArgs e)
        {
            if (txtContraseña.Text == txtConfirmar.Text && comboBox1.SelectedIndex != 0)
            {
                Users k = new Users();
                k.Usuario = txtUsuario.Text;
                k.Contra = txtContraseña.Text;
                k.Down = true;
                k.Rol = comboBox1.Text;
                if (FuncionesUser.CrearCuentas(txtUsuario.Text, txtContraseña.Text, comboBox1.Text) > 0)
                {
                    MessageBox.Show("Cuenta creada con exito");
                    clear();
                }
                else
                {
                    MessageBox.Show("No se pudo crear la cuenta", "Error");
                    clear();
                }
            }
            else
            {
                MessageBox.Show("Las contraseñas no coinciden", "Error");
                clear();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }
    }
}
