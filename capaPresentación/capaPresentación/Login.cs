﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using capaDatos;
using capaEntidades;


namespace capaPresentación
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (FuncionesUser.Autentificar(txtUsuario.Text, txtContraseña.Text) > 0)
            {
                try
                {
                    MDIParent1 main = new MDIParent1();
                    main.Show();
                    this.Hide();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al abrir la Base de datos" + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Usuario y/o contraseña incorrectos. Verifique sus datos", "Error", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        private void Login_Load(object sender, EventArgs e)
        {

        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
