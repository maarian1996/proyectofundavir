﻿namespace capaPresentación
{
    partial class MDIParent1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.gestionUsuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bajasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.modificacionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.listadoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosBloqueadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestiónProfesoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.altaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profesorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bajaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.profesorToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.modificaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnoToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.profesorToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.listadoToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profesoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asistenciasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.profesoresToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.sistemaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.menuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip
            // 
            this.menuStrip.BackColor = System.Drawing.Color.Lavender;
            this.menuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionUsuToolStripMenuItem,
            this.gestiónProfesoresToolStripMenuItem,
            this.sistemaToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip.Size = new System.Drawing.Size(919, 37);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // gestionUsuToolStripMenuItem
            // 
            this.gestionUsuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altasToolStripMenuItem,
            this.bajasToolStripMenuItem,
            this.modificacionToolStripMenuItem,
            this.listadoToolStripMenuItem,
            this.usuariosBloqueadosToolStripMenuItem});
            this.gestionUsuToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestionUsuToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.gestionUsuToolStripMenuItem.Name = "gestionUsuToolStripMenuItem";
            this.gestionUsuToolStripMenuItem.Size = new System.Drawing.Size(209, 33);
            this.gestionUsuToolStripMenuItem.Text = "Gestión Usuarios";
            // 
            // altasToolStripMenuItem
            // 
            this.altasToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.altasToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.altasToolStripMenuItem.Name = "altasToolStripMenuItem";
            this.altasToolStripMenuItem.Size = new System.Drawing.Size(317, 34);
            this.altasToolStripMenuItem.Text = "Altas";
            this.altasToolStripMenuItem.Click += new System.EventHandler(this.altasToolStripMenuItem_Click);
            // 
            // bajasToolStripMenuItem
            // 
            this.bajasToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.bajasToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.bajasToolStripMenuItem.Name = "bajasToolStripMenuItem";
            this.bajasToolStripMenuItem.Size = new System.Drawing.Size(317, 34);
            this.bajasToolStripMenuItem.Text = "Bajas";
            // 
            // modificacionToolStripMenuItem
            // 
            this.modificacionToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.modificacionToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.modificacionToolStripMenuItem.Name = "modificacionToolStripMenuItem";
            this.modificacionToolStripMenuItem.Size = new System.Drawing.Size(317, 34);
            this.modificacionToolStripMenuItem.Text = "Modificación";
            // 
            // listadoToolStripMenuItem
            // 
            this.listadoToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.listadoToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.listadoToolStripMenuItem.Name = "listadoToolStripMenuItem";
            this.listadoToolStripMenuItem.Size = new System.Drawing.Size(317, 34);
            this.listadoToolStripMenuItem.Text = "Listado";
            // 
            // usuariosBloqueadosToolStripMenuItem
            // 
            this.usuariosBloqueadosToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.usuariosBloqueadosToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.usuariosBloqueadosToolStripMenuItem.Name = "usuariosBloqueadosToolStripMenuItem";
            this.usuariosBloqueadosToolStripMenuItem.Size = new System.Drawing.Size(317, 34);
            this.usuariosBloqueadosToolStripMenuItem.Text = "Usuarios Bloqueados";
            // 
            // gestiónProfesoresToolStripMenuItem
            // 
            this.gestiónProfesoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.altaToolStripMenuItem,
            this.bajaToolStripMenuItem,
            this.modificaciónToolStripMenuItem,
            this.listadoToolStripMenuItem1,
            this.asistenciasToolStripMenuItem1});
            this.gestiónProfesoresToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gestiónProfesoresToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.gestiónProfesoresToolStripMenuItem.Name = "gestiónProfesoresToolStripMenuItem";
            this.gestiónProfesoresToolStripMenuItem.Size = new System.Drawing.Size(195, 33);
            this.gestiónProfesoresToolStripMenuItem.Text = "Gestión Escolar";
            // 
            // altaToolStripMenuItem
            // 
            this.altaToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.altaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnoToolStripMenuItem,
            this.profesorToolStripMenuItem});
            this.altaToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.altaToolStripMenuItem.Name = "altaToolStripMenuItem";
            this.altaToolStripMenuItem.Size = new System.Drawing.Size(222, 34);
            this.altaToolStripMenuItem.Text = "Alta";
            // 
            // alumnoToolStripMenuItem
            // 
            this.alumnoToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.alumnoToolStripMenuItem.Name = "alumnoToolStripMenuItem";
            this.alumnoToolStripMenuItem.Size = new System.Drawing.Size(178, 34);
            this.alumnoToolStripMenuItem.Text = "Alumno";
            this.alumnoToolStripMenuItem.Click += new System.EventHandler(this.alumnoToolStripMenuItem_Click);
            // 
            // profesorToolStripMenuItem
            // 
            this.profesorToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.profesorToolStripMenuItem.Name = "profesorToolStripMenuItem";
            this.profesorToolStripMenuItem.Size = new System.Drawing.Size(178, 34);
            this.profesorToolStripMenuItem.Text = "Profesor";
            this.profesorToolStripMenuItem.Click += new System.EventHandler(this.profesorToolStripMenuItem_Click);
            // 
            // bajaToolStripMenuItem
            // 
            this.bajaToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.bajaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnoToolStripMenuItem1,
            this.profesorToolStripMenuItem1});
            this.bajaToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.bajaToolStripMenuItem.Name = "bajaToolStripMenuItem";
            this.bajaToolStripMenuItem.Size = new System.Drawing.Size(222, 34);
            this.bajaToolStripMenuItem.Text = "Baja";
            // 
            // alumnoToolStripMenuItem1
            // 
            this.alumnoToolStripMenuItem1.Name = "alumnoToolStripMenuItem1";
            this.alumnoToolStripMenuItem1.Size = new System.Drawing.Size(178, 34);
            this.alumnoToolStripMenuItem1.Text = "Alumno";
            // 
            // profesorToolStripMenuItem1
            // 
            this.profesorToolStripMenuItem1.Name = "profesorToolStripMenuItem1";
            this.profesorToolStripMenuItem1.Size = new System.Drawing.Size(178, 34);
            this.profesorToolStripMenuItem1.Text = "Profesor";
            // 
            // modificaciónToolStripMenuItem
            // 
            this.modificaciónToolStripMenuItem.BackColor = System.Drawing.Color.Lavender;
            this.modificaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnoToolStripMenuItem2,
            this.profesorToolStripMenuItem2});
            this.modificaciónToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.modificaciónToolStripMenuItem.Name = "modificaciónToolStripMenuItem";
            this.modificaciónToolStripMenuItem.Size = new System.Drawing.Size(222, 34);
            this.modificaciónToolStripMenuItem.Text = "Modificación";
            // 
            // alumnoToolStripMenuItem2
            // 
            this.alumnoToolStripMenuItem2.Name = "alumnoToolStripMenuItem2";
            this.alumnoToolStripMenuItem2.Size = new System.Drawing.Size(178, 34);
            this.alumnoToolStripMenuItem2.Text = "Alumno";
            // 
            // profesorToolStripMenuItem2
            // 
            this.profesorToolStripMenuItem2.Name = "profesorToolStripMenuItem2";
            this.profesorToolStripMenuItem2.Size = new System.Drawing.Size(178, 34);
            this.profesorToolStripMenuItem2.Text = "Profesor";
            // 
            // listadoToolStripMenuItem1
            // 
            this.listadoToolStripMenuItem1.BackColor = System.Drawing.Color.Lavender;
            this.listadoToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnosToolStripMenuItem,
            this.profesoresToolStripMenuItem});
            this.listadoToolStripMenuItem1.ForeColor = System.Drawing.Color.Purple;
            this.listadoToolStripMenuItem1.Name = "listadoToolStripMenuItem1";
            this.listadoToolStripMenuItem1.Size = new System.Drawing.Size(222, 34);
            this.listadoToolStripMenuItem1.Text = "Listado";
            // 
            // alumnosToolStripMenuItem
            // 
            this.alumnosToolStripMenuItem.Name = "alumnosToolStripMenuItem";
            this.alumnosToolStripMenuItem.Size = new System.Drawing.Size(204, 34);
            this.alumnosToolStripMenuItem.Text = "Alumnos";
            // 
            // profesoresToolStripMenuItem
            // 
            this.profesoresToolStripMenuItem.Name = "profesoresToolStripMenuItem";
            this.profesoresToolStripMenuItem.Size = new System.Drawing.Size(204, 34);
            this.profesoresToolStripMenuItem.Text = "Profesores";
            // 
            // asistenciasToolStripMenuItem1
            // 
            this.asistenciasToolStripMenuItem1.BackColor = System.Drawing.Color.Lavender;
            this.asistenciasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.alumnosToolStripMenuItem1,
            this.profesoresToolStripMenuItem1});
            this.asistenciasToolStripMenuItem1.ForeColor = System.Drawing.Color.Purple;
            this.asistenciasToolStripMenuItem1.Name = "asistenciasToolStripMenuItem1";
            this.asistenciasToolStripMenuItem1.Size = new System.Drawing.Size(222, 34);
            this.asistenciasToolStripMenuItem1.Text = "Asistencias";
            // 
            // alumnosToolStripMenuItem1
            // 
            this.alumnosToolStripMenuItem1.Name = "alumnosToolStripMenuItem1";
            this.alumnosToolStripMenuItem1.Size = new System.Drawing.Size(204, 34);
            this.alumnosToolStripMenuItem1.Text = "Alumnos ";
            // 
            // profesoresToolStripMenuItem1
            // 
            this.profesoresToolStripMenuItem1.Name = "profesoresToolStripMenuItem1";
            this.profesoresToolStripMenuItem1.Size = new System.Drawing.Size(204, 34);
            this.profesoresToolStripMenuItem1.Text = "Profesores";
            // 
            // sistemaToolStripMenuItem
            // 
            this.sistemaToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sistemaToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.sistemaToolStripMenuItem.Name = "sistemaToolStripMenuItem";
            this.sistemaToolStripMenuItem.Size = new System.Drawing.Size(112, 33);
            this.sistemaToolStripMenuItem.Text = "Sistema";
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ayudaToolStripMenuItem.ForeColor = System.Drawing.Color.Purple;
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(91, 33);
            this.ayudaToolStripMenuItem.Text = "Ayuda";
            // 
            // MDIParent1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Lavender;
            this.ClientSize = new System.Drawing.Size(919, 603);
            this.Controls.Add(this.menuStrip);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "MDIParent1";
            this.ShowIcon = false;
            this.Text = "MDIParent1";
            this.Load += new System.EventHandler(this.MDIParent1_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion


        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem gestionUsuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bajasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificacionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listadoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestiónProfesoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem altaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bajaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem modificaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listadoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem asistenciasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem usuariosBloqueadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alumnoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profesorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alumnoToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem profesorToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alumnoToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem profesorToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem alumnosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem profesoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alumnosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem profesoresToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem sistemaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
    }
}



